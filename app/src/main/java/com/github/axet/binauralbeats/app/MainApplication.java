package com.github.axet.binauralbeats.app;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.preference.PreferenceManager;

import com.github.axet.androidlibrary.widgets.ThemeUtils;
import com.github.axet.binauralbeats.R;
import com.github.axet.binauralbeats.beats.BeatsPlayer;

public class MainApplication extends com.github.axet.androidlibrary.app.MainApplication {
    public static final String PREFERENCE_RATE = "sample_rate";
    public static final String PREFERENCE_CALL = "call";
    public static final String PREFERENCE_SILENT = "silence";
    public static final String PREFERENCE_THEME = "theme";
    public static final String PREFERENCE_BT = "VOLUME_VC";
    public static final String PREFERENCE_BG = "VOLUME_BG";

    public static final String CUSTOM_BACKGROUND = "CUSTOM_BACKGROUND";
    public static final String CUSTOM_DURATION = "CUSTOM_DURATION";
    public static final String CUSTOM_BASE = "CUSTOM_BASE";
    public static final String CUSTOM_BEAT = "CUSTOM_BEAT";
    public static final String CUSTOM_BEATSTART = "CUSTOM_BEATSTART";
    public static final String CUSTOM_BEATEND = "CUSTOM_BEATEND";

    @Override
    public void onCreate() {
        super.onCreate();
        PreferenceManager.setDefaultValues(this, R.xml.pref_general, false);
        Context context = this;
        context.setTheme(getUserTheme());
    }

    public static int getTheme(Context context, int light, int dark) {
        final SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(context);
        String theme = shared.getString(PREFERENCE_THEME, "");
        if (theme.equals(context.getString(R.string.Theme_Dark))) {
            return dark;
        } else {
            return light;
        }
    }

    public static int getActionbarColor(Context context) {
        int colorId = MainApplication.getTheme(context, R.attr.colorPrimary, R.attr.colorButtonNormal);
        int color = ThemeUtils.getThemeColor(context, colorId);
        return color;
    }

    public int getUserTheme() {
        return getTheme(this, R.style.AppThemeLight, R.style.AppThemeDark);
    }

    public static float getFlatBtVol(Context context) {
        final SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(context);
        return shared.getFloat(MainApplication.PREFERENCE_BT, BeatsPlayer.DEFAULT_VOLUME);
    }

    public static float getFlatBgVol(Context context) {
        final SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(context);
        return shared.getFloat(MainApplication.PREFERENCE_BG, BeatsPlayer.DEFAULT_VOLUME * BeatsPlayer.BG_VOLUME_RATIO);
    }
}
