package com.github.axet.binauralbeats.services;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.media.session.MediaButtonReceiver;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.support.v7.view.ContextThemeWrapper;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.RemoteViews;

import com.github.axet.androidlibrary.widgets.NotificationChannelCompat;
import com.github.axet.androidlibrary.widgets.ProximityShader;
import com.github.axet.androidlibrary.widgets.RemoteViewsCompat;
import com.github.axet.androidlibrary.widgets.ThemeUtils;
import com.github.axet.binauralbeats.R;
import com.github.axet.binauralbeats.activities.MainActivity;
import com.github.axet.binauralbeats.app.MainApplication;
import com.github.axet.binauralbeats.app.Sound;

/**
 * RecordingActivity more likly to be removed from memory when paused then service. Notification button
 * does not handle getActvity without unlocking screen. The only option is to have Service.
 * <p/>
 * So, lets have it.
 * <p/>
 * Maybe later this class will be converted for fully feature recording service with recording thread.
 */
public class BeatsService extends Service implements SharedPreferences.OnSharedPreferenceChangeListener {
    public static final String TAG = BeatsService.class.getSimpleName();

    public static final int NOTIFICATION_RECORDING_ICON = 1;

    public static String SHOW_ACTIVITY = BeatsService.class.getCanonicalName() + ".SHOW_ACTIVITY";
    public static String PAUSE_BUTTON = BeatsService.class.getCanonicalName() + ".PAUSE_BUTTON";
    public static String STOP_BUTTON = BeatsService.class.getCanonicalName() + ".STOP_BUTTON";

    RecordingReceiver receiver;

    String name;
    String time;
    boolean playing;
    boolean end;
    Sound sound;
    MediaSessionCompat ms;
    PendingIntent pause;
    PendingIntent stop;
    Notification notification;

    public class RecordingReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            MediaButtonReceiver.handleIntent(ms, intent);
        }
    }

    public static void startService(Context context, String name, String time, boolean playing, boolean end) {
        context.startService(new Intent(context, BeatsService.class)
                .putExtra("name", name)
                .putExtra("time", time)
                .putExtra("playing", playing)
                .putExtra("end", end)
        );
    }

    public static void stopService(Context context) {
        context.stopService(new Intent(context, BeatsService.class));
    }

    public BeatsService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate");

        pause = PendingIntent.getService(this, 0,
                new Intent(this, BeatsService.class).setAction(PAUSE_BUTTON),
                PendingIntent.FLAG_UPDATE_CURRENT);

        stop = PendingIntent.getService(this, 0,
                new Intent(this, BeatsService.class).setAction(STOP_BUTTON),
                PendingIntent.FLAG_UPDATE_CURRENT);

        sound = new Sound(this);

        receiver = new RecordingReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_MEDIA_BUTTON);
        registerReceiver(receiver, filter);

        startForeground(NOTIFICATION_RECORDING_ICON, buildNotification());

        SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(this);
        shared.registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            String a = intent.getAction();
            if (a == null) {
                name = intent.getStringExtra("name");
                time = intent.getStringExtra("time");
                playing = intent.getBooleanExtra("playing", false);
                end = intent.getBooleanExtra("end", false);

                if (playing)
                    sound.silent();
                else
                    sound.unsilent();

                showNotificationAlarm(true);
            } else if (a.equals(PAUSE_BUTTON)) {
                Intent i = new Intent(MainActivity.PAUSE_BUTTON);
                sendBroadcast(i);
            } else if (a.equals(STOP_BUTTON)) {
                Intent i = new Intent(MainActivity.STOP_BUTTON);
                sendBroadcast(i);
            } else if (a.equals(SHOW_ACTIVITY)) {
                ProximityShader.closeSystemDialogs(this);
                MainActivity.startActivity(this);
            }
        } // else restart
        return super.onStartCommand(intent, flags, startId);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public class Binder extends android.os.Binder {
        public BeatsService getService() {
            return BeatsService.this;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestory");

        sound.unsilent();

        showNotificationAlarm(false);

        unregisterReceiver(receiver);

        SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(this);
        shared.unregisterOnSharedPreferenceChangeListener(this);
    }

    public static boolean getAttr(AttributeSet attrs, String name, TypedValue out) {
        for (int i = 0; i < attrs.getAttributeCount(); i++) {
            if (attrs.getAttributeName(i).equals(name)) {
                String v = attrs.getAttributeValue(i);
                int pos = 0;
                switch (v.charAt(pos)) {
                    case '?':
                        out.type = TypedValue.TYPE_ATTRIBUTE;
                        pos++;
                        break;
                    case '@':
                        out.type = TypedValue.TYPE_REFERENCE;
                        pos++;
                        break;
                }
                v = v.substring(pos);
                return true;
            }
        }
        return false;
    }

    @SuppressLint("RestrictedApi")
    Notification buildNotification() {
        PendingIntent main = PendingIntent.getService(this, 0,
                new Intent(this, BeatsService.class).setAction(SHOW_ACTIVITY),
                PendingIntent.FLAG_UPDATE_CURRENT);

        RemoteViews view = new RemoteViews(getPackageName(), MainApplication.getTheme(this, R.layout.notifictaion_recording_light, R.layout.notifictaion_recording_dark));

        ContextThemeWrapper theme = new ContextThemeWrapper(this, MainApplication.getTheme(this, R.style.AppThemeLight, R.style.AppThemeDark));
        RemoteViewsCompat.setImageViewTint(view, R.id.icon_circle, ThemeUtils.getThemeColor(theme, R.attr.colorButtonNormal)); // android:tint="?attr/colorButtonNormal" not working API16
        RemoteViewsCompat.applyTheme(theme, view);

        boolean pause = !playing && !end;
        String title = name;

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);

        if (title == null || title.isEmpty()) {
            title = getString(R.string.app_name);
            time = getString(R.string.optimization_killed);
            view.setViewVisibility(R.id.notification_pause, View.GONE);
            view.setViewVisibility(R.id.notification_stop, View.GONE);
            builder.setOngoing(false);
            headset(false, playing);
        } else {
            view.setViewVisibility(R.id.notification_pause, View.VISIBLE);
            view.setViewVisibility(R.id.notification_stop, View.VISIBLE);
            if (pause)
                title += getString(R.string.pause_notify);
            else if (end)
                title += getString(R.string.end_notify);
            else
                title += "…";
            builder.setOngoing(!end); // ongoing until end
            builder.setWhen(notification == null ? System.currentTimeMillis() : notification.when);
            headset(true, playing);
        }

        view.setOnClickPendingIntent(R.id.status_bar_latest_event_content, main);
        view.setTextViewText(R.id.notification_title, title);
        view.setTextViewText(R.id.notification_text, time);
        view.setOnClickPendingIntent(R.id.notification_pause, this.pause);
        view.setOnClickPendingIntent(R.id.notification_stop, this.stop);
        view.setImageViewResource(R.id.notification_pause, !playing ? R.drawable.ic_play_arrow_black_24dp : R.drawable.ic_pause_black_24dp);

        builder.setOnlyAlertOnce(true)
                .setShowWhen(false)
                .setContentTitle(title)
                .setSmallIcon(R.drawable.ic_stat_name)
                .setContentText(time)
                .setContent(view);

        if (Build.VERSION.SDK_INT < 11)
            builder.setContentIntent(main);

        if (Build.VERSION.SDK_INT >= 21)
            builder.setVisibility(NotificationCompat.VISIBILITY_PUBLIC);

        Notification n = builder.build();
        NotificationChannelCompat channel = new NotificationChannelCompat(this, "playing", "Playing", NotificationManagerCompat.IMPORTANCE_LOW);
        channel.apply(n);
        return n;
    }

    // alarm dismiss button
    public void showNotificationAlarm(boolean show) {
        NotificationManagerCompat nm = NotificationManagerCompat.from(this);
        if (!show) {
            stopForeground(false);
            nm.cancel(NOTIFICATION_RECORDING_ICON);
            notification = null;
        } else {
            Notification n = buildNotification();
            if (notification == null)
                startForeground(NOTIFICATION_RECORDING_ICON, n);
            else
                nm.notify(NOTIFICATION_RECORDING_ICON, n);
            notification = n;
        }
    }

    void headset(boolean b, boolean playing) {
        if (b) {
            if (ms == null) {
                ms = new MediaSessionCompat(this, TAG, new ComponentName(this, RecordingReceiver.class), null);
                ms.setCallback(new MediaSessionCompat.Callback() {
                    @Override
                    public void onPlay() {
                        pause();
                    }

                    @Override
                    public void onPause() {
                        pause();
                    }

                    @Override
                    public void onStop() {
                        try {
                            stop.send();
                        } catch (PendingIntent.CanceledException e) {
                            Log.d(TAG, "pause canceled", e);
                        }
                    }
                });
            }
            PlaybackStateCompat state = new PlaybackStateCompat.Builder()
                    .setActions(PlaybackStateCompat.ACTION_PLAY | PlaybackStateCompat.ACTION_PAUSE | PlaybackStateCompat.ACTION_PLAY_PAUSE | PlaybackStateCompat.ACTION_STOP)
                    .setState(playing ? PlaybackStateCompat.STATE_PLAYING : PlaybackStateCompat.STATE_PAUSED, 0, 1)
                    .build();
            ms.setPlaybackState(state);
            ms.setFlags(MediaSessionCompat.FLAG_HANDLES_MEDIA_BUTTONS | MediaSessionCompat.FLAG_HANDLES_TRANSPORT_CONTROLS);
            ms.setActive(true);
        } else {
            if (ms != null) {
                ms.release();
                ms = null;
            }
        }
    }

    void pause() {
        try {
            pause.send();
        } catch (PendingIntent.CanceledException e) {
            Log.d(TAG, "pause canceled", e);
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(MainApplication.PREFERENCE_THEME)) {
        }
    }

}
